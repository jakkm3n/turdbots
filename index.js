'use strict';
const app = require('express')();
const R = require('ramda');
const Immutable = require('immutable');
const irc = require('irc');
const request = require('request');

const conf = Immutable.Map(require('yamljs').load('./config.yml'));

function* arrayCycler(arr) {
    let i = 0;
    while (true) {
        i = (i === arr.size) ? 0 : i;
        yield arr.get(i++);
    }
}

const serverCycler = arrayCycler(Immutable.List(conf.get('servers')));

const makeBot = (nick) => new irc.Client(serverCycler.next().value, nick, {
    userName: nick,
    realName: nick,
    channels: [conf.get('channel')],
    autoRejoin: true,
    autoConnect: true, // this may need to be turned off
    encoding: 'UTF-8'
});

const botCycler = R.pipe(
    R.map(makeBot),
    Immutable.List,
    arrayCycler
)(conf.get('nicks'));

let lock = false; // i want this to go away

const pump = (ascii, i, delay, bot) => {
    setTimeout(() => bot.say(conf.get('channel'), ascii.get(i)), delay);

    if (i + 1 == ascii.size) {
        setTimeout(() => lock = false, delay);
        return 'h';
    }

    return (i % 2 === 0) ?
        pump(ascii, i + 1, delay + 10, bot):
        pump(ascii, i + 1, delay + conf.get('interval'), botCycler.next().value);
}

app.get('/pump/:password/:ascii', (req, res) => {
    request(conf.get('asciiUrl') + req.params.ascii + '.txt', (error, response, body) => {
        if (req.params.password !== conf.get('password')) {
            res.send('wrong password');
        } else if (lock) {
            res.send('an ascii is already being pumped lol');
        } else if (!error && response.statusCode == 200) {
            lock = true;

            pump(Immutable.List(body.split("\n")), 0, conf.get('interval'), botCycler.next().value);

            res.send('ascii pumped');
            console.log('pumping ' + req.params.ascii + '.txt');
        } else {
            res.send('couldnt find ' + req.params.ascii + '.txt');
        }
    });
});

const server = app.listen(conf.get('serverPort'), () => console.log('listening at', server.address().port));
